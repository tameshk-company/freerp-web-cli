import React, {Component} from "react";
import {Button, Card, Checkbox, Col, Form, Input, Row} from "antd";

const {Item} = Form;

const tailLayout = {
    wrapperCol: {offset: 5, span: 16},
    textAlign: "left"
};


export default class Login extends Component{

    componentDidMount() {

    }

    login(values: any) {
        console.log(values);
    }

    validationWarning(errorInfo: any) {
        console.log(errorInfo);
    }


    render() {
        return (<Row justify="center"  align="middle" style={{height: "100%"}}>
            <Col xs={18} sm={16} md={12} lg={10} xl={8}>
                <Card title="Login" style={{textAlign: "center"}}>
                    <Form
                        name="basic"
                        initialValues={{remember: true}}
                        onFinish={(values) => this.login(values)}
                        onFinishFailed={(errorInfo) => this.validationWarning(errorInfo)}
                    >
                        <Item
                            label="Username"
                            name="username"
                            rules={[{required: true, message: 'Please input your username!'}]}
                        >
                            <Input/>
                        </Item>

                        <Item
                            label="Password"
                            name="password"
                            rules={[{required: true, message: 'Please input your password!'}]}>
                            <Input.Password/>
                        </Item>

                        <Item {...tailLayout} style={{textAlign: "left"}} name="remember" valuePropName="checked">
                            <Checkbox>Remember me</Checkbox>
                        </Item>

                        <Item>
                            <Button type="primary" htmlType="submit" style={{width: "100%"}}>
                                Login
                            </Button>
                        </Item>
                    </Form>
                </Card>
            </Col>
        </Row>);
    }
}