import React, {Component} from "react";
import DataTable, {ColumnType} from "../Component/DataTable";
import {Row, Col, PageHeader, Card, Button, Form} from "antd";
import {FormInstance} from "antd/lib/form";
import constants from "../constants";


interface Item {
    _id: number;
    title: string;
    symbol: string;
}

class Currency extends Component {

    filterFormRef = React.createRef<FormInstance>();
    areaTableRef = React.createRef<CurrencyTable>();

    state = {
        filters: {}
    }

    filterFormChanged(changedFields: any[], allFields: any[]) {
        this.setState({filters: Object.assign({}, this.filterFormRef.current?.getFieldsValue())});
    }

    sortBy: string | null = null;

    onSortChange(sorter: { field: string, order: string }) {
        let filters: any = Object.assign({}, this.state.filters)
        if (this.sortBy !== null) {
            delete filters[this.sortBy];
        }
        if (sorter.order === 'ascend') {
            this.sortBy = sorter.field;
            filters[sorter.field] = '~{"sort":{"index": 1}}';
        } else if (sorter.order === 'descend') {
            this.sortBy = sorter.field;
            filters[sorter.field] = '~{"sort":{"index": 1, "desc": true}}';
        } else {
            this.sortBy = null;
        }
        this.setState({filters: filters})
    }

    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <PageHeader title='Currency'/>
                    </Col>
                </Row>
                <Row gutter={[32, 16]} className='filters'>
                    <Col flex={10}>
                        <Card style={{height: '80px'}}>
                            <Row>
                                <Col flex={10}>
                                    <Form
                                        layout="inline"
                                        name="filters"
                                        ref={this.filterFormRef}
                                        initialValues={this.state.filters}
                                        onValuesChange={(changedFields: any, allFields: any) => this.filterFormChanged(changedFields, allFields)}
                                    >
                                    </Form>
                                </Col>
                                <Col>
                                    <Button type='primary' title='New'
                                            onClick={() => this.areaTableRef.current?.addItem()}>New Item</Button>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col flex={10}>
                        <CurrencyTable ref={this.areaTableRef} filters={this.state.filters}
                                       onSortChange={(sorter: { field: string, order: string }) => {
                                           this.onSortChange(sorter)
                                       }}/>
                    </Col>
                </Row>
            </div>
        )
    }
}

export class CurrencyTable extends DataTable<Item, { filters: Record<string, any>, onSortChange: (sorter: { field: string, order: string }) => void }> {
    async componentDidMount() {
        await super.componentDidMount();
        this.load();
        const columns: ColumnType[] = [
            {
                title: 'Title',
                dataIndex: 'title',
                dataType: "text",
                editable: true,
                rules: [{required: true}],
                sorter: true
            },
            {
                title: 'Symbol',
                dataIndex: 'symbol',
                dataType: "text",
                editable: true,
                rules: [{required: true}],
                sorter: true
            }
        ];
        await this.setColumns(columns);
    }


    load() {
        const filters = Object.fromEntries(
            Object.entries(this.props.filters).map(
                ([k, v]) => k === 'title' ? (v ? [k, '~{"filters":{"like":"%' + v + '%"}}'] : []) : [k, v]
            )
        )

        this.setState({loading: true})

        console.log("URL", constants.API_URL);
        fetch(constants.API_URL + "/Currency/get", {
            method: "POST",
            body: JSON.stringify({filters: filters})
        })
            .then(res => res.json())
            .then(
                (result: any) => {
                    this.setState({loading: false})
                    if (result.status === 'success') {
                        /*for (const i in result.data[1])
                            result.data[1][i].id = result.data[1][i].id;*/
                        this.setState({data: result.data[1]});
                        this.setState({pagination: {total: result.data[0]}});
                    }

                }
            );
    }


    onChange(pagination: any, filters: any, sorter: { field: string, order: string }, extra: any): void {
        if (extra.action === 'sort') {
            this.props.onSortChange(sorter);
        }
        this.load();
    }


    model = "Currency";


}

export default Currency;
