export default {
    getFormData: (object: Record<string,(string|Blob)>) => Object.entries(object).reduce((formData, [key,value])=>{
        formData.append(key, value);
        return formData;
    }, new FormData())

}