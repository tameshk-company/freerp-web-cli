import React, {Component} from "react";
import {Cascader} from "antd";
import {CascaderOptionType, CascaderValueType} from "antd/lib/cascader";
import {ReloadOutlined} from "@ant-design/icons"

type ResultValue = { id: number, title?: string, child?: ResultValue, level: number };

export interface CascaderFieldProps {
    value?: any;
    onChange?: (value: ResultValue | null) => void;
    model?: string,
    options?: CascaderOptionType[]
    loadData: (selectedOptions: CascaderOptionType[]) => Promise<CascaderOptionType[]>,
    subObjKeys?: string[],
    reload?: () => void;
}

export default class Cascader2 extends Component<CascaderFieldProps> {


    state: {
        options: CascaderOptionType[],
        value: number[],
        selectedOptionLevels: CascaderOptionType[]
    } = {
        options: [],
        value: [],
        selectedOptionLevels: []
    }

    async componentDidMount() {
        await this.setState({options: this.props.options})
        console.log("this.props.value", this.props.value)
        if (this.props.value) {
            let obj: any = this.props.value;
            let firstObj: any = obj;
            let value = [obj.id]

            let selectedOptionLevels: CascaderOptionType[] = []
            const options = [...this.state.options]
            if (this.props.subObjKeys)
                for (let i = this.props.subObjKeys.length - 1; i >= 0; i--) {
                    selectedOptionLevels.push({value: obj.id, label: obj.title, type: this.props.subObjKeys[i + 1]})
                    obj = obj[this.props.subObjKeys[i]];
                    value.unshift(obj.id)
                }
            console.log("this.setState({value: value})", value)
            await this.setState({value: value})

            let optionCursor = options.find((option) => {
                return option.value == value[0]
            });
            let optionDup = optionCursor;
            if (this.props.subObjKeys)
                for (let i = 0; i < this.props.subObjKeys.length; i++) {
                    if (optionCursor) {
                        optionCursor.children = [selectedOptionLevels[this.props.subObjKeys.length - i - 1]]
                        optionCursor.shouldBeReload = true;
                        optionCursor.isLeaf = false;
                        optionCursor = optionCursor.children[0]
                    }
                }
            options.map((option) => {
                if (option.value == obj.id) {
                    return optionDup;
                }
                return option;
            })
            selectedOptionLevels.push({value: obj.id, isLeaf: false, label: obj.title})
            this.setState({selectedOptionLevels: selectedOptionLevels.reverse()})
            await this.setState({options: options})
        }

    }

    triggerChange = (changedValue: CascaderValueType, selectedOptions?: CascaderOptionType[]) => {
        this.setState({value: changedValue});
        if (this.props.onChange) {
            let i = 0;
            let selectedOption = this.props.options?.find((item) => item.value === changedValue[i])
            if (selectedOptions === undefined || !selectedOption) {
                this.props.onChange(null);
                return;
            } else if (!selectedOption.value) {
                this.props.onChange(null);
                return;
            } else {
                let val;
                if (typeof selectedOption.value === 'string') {
                    val = Number.parseInt(selectedOption.values)
                } else {
                    val = selectedOption.value;
                }
                let obj: ResultValue = {id: val, title: selectedOption.label?.toString(), level: i}
                let option: CascaderOptionType | undefined = selectedOption;
                while (option && Array.isArray(selectedOption.children) && changedValue[++i]) {
                    let childObj = Object.assign({}, obj)
                    option = option.children?.find((item) => item.value === changedValue[i])
                    if (option && option.value) {
                        obj = {
                            id: typeof option.value === 'string' ? Number.parseInt(option.value) : option.value,
                            title: option.label?.toString(),
                            level: i
                        }
                    }
                    // @ts-ignore
                    obj[this.props.subObjKeys[i - 1]] = childObj;
                }
                this.props.onChange(obj);
            }
        }
    };


    async loadData(selectedOptions?: CascaderOptionType[]) {
        if (selectedOptions) {
            const targetOption = selectedOptions[selectedOptions.length - 1];
            targetOption.loading = true;
            targetOption.children = await this.props.loadData(selectedOptions)
            targetOption.loading = false;
            this.setState({
                options: [...this.state.options],
            });
        }
    };

    reload(){
        if(this.props.reload)
            this.props.reload()
    }

    cascaderRef = React.createRef<Cascader>();

    async popupVisibleChangeHandle(value: any) {
        if (value) {
            const checkAndLoadChildren = async (options: CascaderOptionType[], selectedOptions: CascaderOptionType[]) => {
                const promises = options.map(async (option) => {
                    if (option.shouldBeReload) {
                        option.loading = true;
                        delete option.shouldBeReload;
                        const selectedForChildren = this.state.selectedOptionLevels.slice(0, selectedOptions.length + 1)
                        option.children = await this.props.loadData(selectedOptions)
                        option.children = option.children.map((option) => {
                            if (option.value == selectedForChildren[selectedForChildren.length - 1].value) {
                                option.shouldBeReload = true
                            }
                            return option
                        })
                        if (selectedForChildren.length < this.state.selectedOptionLevels.length) {
                            option.children = await checkAndLoadChildren(option.children, selectedForChildren)
                        }
                        option.loading = false;
                    }
                    return option
                })
                return await Promise.all(promises)
            }
            let options = [...this.state.options]
            options = await checkAndLoadChildren(options, [this.state.selectedOptionLevels[0]])
            await this.setState({options: options})
        }
    }

    render() {
        return <>
            <Cascader onPopupVisibleChange={(value) => this.popupVisibleChangeHandle(value)} ref={this.cascaderRef}
                      changeOnSelect
                      value={this.state.value} onChange={this.triggerChange}
                      options={this.state.options} loadData={(selectedOptions) => this.loadData(selectedOptions)}/>
            <ReloadOutlined onClick={()=>this.reload()} size={24} color={'#444'}
                            style={{position: 'absolute', left: '32px', top: '8px', userSelect: "none", cursor: 'pointer'}}/>
        </>
    }
}