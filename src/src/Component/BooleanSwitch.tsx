import React, {Component} from "react";
import {Switch} from "antd";

export interface BooleanFieldProps {
    value?: boolean;
    onChange?: (value: boolean) => void;
}

export default class BooleanSwitch extends Component<BooleanFieldProps> {

    constructor(props: BooleanFieldProps) {
        super(props);
    }

    triggerChange = (changedValue: any) => {
        if (this.props.onChange) {
            console.log('changedValue:', changedValue);
            this.props.onChange(changedValue);
        }
    };


    render() {
        return (<Switch checked={this.props.value} onChange={this.triggerChange}/>)
    }
}