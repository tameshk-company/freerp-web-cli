import {Component} from "react";
import React from "react"
import Upload from "antd/lib/upload";
import {
    UploadOutlined, LoadingOutlined
} from '@ant-design/icons';
import {UploadChangeParam, RcFile} from "antd/lib/upload/interface";
import {UploadFile} from "antd/es/upload/interface";

async function delay(ms: number) {
    return await new Promise(resolve => setTimeout(resolve, ms));
}

export interface UploadFieldProps {
    value?: number;
    onChange?: (value: number) => void;
    getDoUpload?: (waitForUpload: (that: any) => Promise<boolean>, that: any) => void,
    data: any,
    previewURL: string,
    record: any
}

function getBase64(file: Blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

export default class Upload2 extends Component<UploadFieldProps> {

    state = {
        uploadingImage: false,
        uploadIsWaiting: false,
        uploading: false,
        preview: ''
    }

    componentDidMount(): void {
        if (this.props.getDoUpload)
            this.props.getDoUpload(this.doUpload, this)
    }

    tempId?: number;

    async doUpload(that: any, withId?: number) {
        if (withId) {
            that.tempId = withId;
        }
        console.log("this.state.uploadIsWaiting", that.state.uploadIsWaiting, "WithId", withId)
        if (that.state.uploadIsWaiting) {
            await that.setState({uploadIsWaiting: false, uploading: true});
            while (that.state.uploading) {
                await delay(300)
            }

            console.log(that.props.onChange)
            if (that.props.onChange && that.props.value != undefined) {
                that.props.onChange(Number.parseInt(that.props.value) + 1)
            }
            return true
        }
        return false
    }

    imageUploadHandleChange(info: UploadChangeParam) {
        const that = this;
        if (info.file.status === 'uploading') {
            this.setState({uploadingImage: true});
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            const reader = new FileReader();
            reader.addEventListener('load', async _ =>
                await that.setState({
                    uploadingImage: false,
                }),
            )
            if (info.file.originFileObj !== undefined) {
                reader.readAsDataURL(info.file.originFileObj);
            }


            //this.formRef.current.setFieldsValue({imgVersion: record.imgVersion + 1})
            that.setState({uploading: false});
        }
    }

    async beforeUpload(options: RcFile) {
        await this.setState({uploadIsWaiting: true})
        console.log("uploadIsWaiting", this.state.uploadIsWaiting)


        const result = await getBase64(options.slice());
        if (typeof result === "string") {
            this.setState({preview: result})
        }

        while (this.state.uploadIsWaiting) {
            await delay(300)
        }
        console.log("Upload start", this.state.uploadIsWaiting)

    }

    async imageUploadHandlePreview(file: UploadFile<any>) {
        console.log("ImageUpload HandlePreview")
        if (!file.url && !file.preview && file.originFileObj instanceof File) {
            const result = await getBase64(file.originFileObj);
            if (typeof result === "string") {
                file.preview = result
                this.setState({preview: result})
            }
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
            previewTitle: file.url ? (file.name || file.url.substring(file.url.lastIndexOf('/'))) : "",
        });
    }

    get data() {
        const result = Object.assign({}, this.props.data)
        result.path = result.path.replace("(%imgVersion%)", (Number.parseInt(this.props.value ? this.props.value.toString() : "0") + 1) + "").replace("(%id%)", this.tempId ? this.tempId : this.props.record.id)
        console.log("getData from upload2", result.path)
        return result
    }

    get previewURL() {
        return this.props.previewURL.replace("(%imgVersion%)", this.props.value + "").replace("(%id%)", this.tempId ? this.tempId : this.props.record.id)
    }

    render() {
        return (
            <Upload
                action="../api/upload.php"
                data={this.data}
                listType="picture-card"
                showUploadList={false}
                beforeUpload={async (options: RcFile) => this.beforeUpload(options)}
                onPreview={(file) => this.imageUploadHandlePreview(file)}
                onChange={(event: UploadChangeParam) => {
                    this.imageUploadHandleChange(event)
                }}
                style={{position: 'relative'}}
            >
                <img
                    src={(this.state.uploadIsWaiting || this.state.uploading) ? this.state.preview : this.previewURL}
                    alt="avatar"
                    style={{
                        height: 100,
                        position: "absolute",
                        right: "50%",
                        top: "50%",
                        transform: "translate(50%,-50%)"
                    }}/>
                <div style={{
                    position: "absolute",
                    right: "50%",
                    top: "50%",
                    transform: "translate(50%,-50%)"
                }}>
                    {this.state.uploadingImage ? <LoadingOutlined/> : <UploadOutlined/>}
                    <div style={{marginTop: 8}}>آپلود</div>
                </div>
            </Upload>
        )
    }
}