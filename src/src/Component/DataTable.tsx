import React, {Component} from "react";
import {Input, InputNumber, Table, Form, Popconfirm, Button, Switch, notification, Popover} from "antd";
import {
    DeleteOutlined,
    EditOutlined,
    SaveOutlined,
    CloseOutlined
} from '@ant-design/icons';
import BooleanSwitch from "./BooleanSwitch";
import Cascader2 from "./Cascader2";
import {CascaderOptionType} from "antd/lib/cascader";
import Select2 from "./Select2";
import {RuleObject} from "rc-field-form/lib/interface"
import {FormInstance} from "antd/lib/form";
import Upload2 from "./Upload2";
import TableInterfaces from "antd/lib/table/interface"
import constants from "../constants";
import BraftModal from "./BraftModal";
import utils from "../utils";

export type DataType = 'number' | 'text' | 'boolean' | 'cascader' | 'operation' | 'selection' | 'imageUpload' | 'html';
// export type ColumnType = {
//     title: string,
//     dataIndex: string,
//     dataType: DataType,
//     width?: string | number,
//     editable?: boolean,
//     sorter?: boolean,
//     rules: RuleObject[],
//     render?: ((_: any, record: any) => {}),
//     extra?: {
//         reload?: () => void, objectValue?: boolean, options?: CascaderOptionType[], cascaderLoadData?: (selectedOptions: CascaderOptionType[]) => Promise<CascaderOptionType[]>, subObjKeys?: string[]
//     }
// };
export interface ColumnType extends TableInterfaces.ColumnType<any> {
    editable?: boolean,
    rules: RuleObject[],
    dataType: DataType,
    dataIndex: string,
    render?: ((_: any, record: any) => {}),
    extra?: {
        reload?: () => void, objectValue?: boolean, options?: CascaderOptionType[], cascaderLoadData?: (selectedOptions: CascaderOptionType[]) => Promise<CascaderOptionType[]>, subObjKeys?: string[]
    },
    shouldCellUpdate?: (record: any, prevRecord: any) => boolean
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
    editing: boolean;
    record: Record<string, any>;
    index: number;
    value: any;
    column: ColumnType
}

abstract class DataTable<Item extends Pick<Record<string, any>, '_id'>, Props> extends Component<Props> {


    state: {
        mergedColumns: any[],
        data: Item[],
        editingKey?: number,
        loading: boolean,
        pagination: {},
        uploadingImage: boolean,
        columns: ColumnType[],
        visibleModal?: string
    } = {
        mergedColumns: [],
        data: [],
        editingKey: undefined,
        loading: false,
        pagination: {total: 0},
        uploadingImage: false,
        columns: [],
    };


    constructor(props: any) {
        super(props);
    }

    //get isEditing (record: Item) {return  record.id === this.state.editingKey || record.id === 0};

    async edit(_id: number) {
        const record = this.state.data.find((item) => item._id === _id)
        if (record && this.formRef.current) {
            await this.setState({editingKey: record._id});
            await this.formRef.current.setFieldsValue(record);
            console.log(this.formRef.current.getFieldsValue())
        }
    };


    doUpload: (that: any, withId?: number) => Promise<boolean> = async () => {
        return false
    };


    async save(_id: React.Key) {
        const newData = [...this.state.data];
        const itemIndex = newData.findIndex(item => _id === item._id);
        const isNew = newData.findIndex(item => 0 === item._id) >= 0;
        if (!isNew) {
            await this.doUpload(this.uploadThis);
        }

        if (isNew) {
            for (const col of this.state.columns.filter((columns) => columns.dataType === "imageUpload")) {
                let fields: any = {};
                fields[col.dataIndex] = 0
                await this.formRef.current?.setFieldsValue(fields);
            }

            console.log("getFieldsValue", this.formRef.current?.getFieldsValue())
        }
        let row;
        try {
            row = (await this.formRef.current?.validateFields()) as Item;
        } catch (errInfo) {
            notification.warn({
                message: 'More information needed',
                description: 'Please enter all needed items.',
            });
            throw errInfo;
        }


        const item = newData[itemIndex];
        newData.splice(itemIndex, 1, {
            ...item,
            ...row,
        });


        this.setState({loading: true});
        const updatingItem = Object.assign({}, newData.find((item) => item._id === _id));
        if (updatingItem === undefined) {
            return;
        }
        const Throw = () => {
            throw false;
        }

        try {
            let res;
            if (isNew) {
                delete updatingItem._id;
                res = await fetch(
                    constants.API_URL + "/" + this.model + "/insert",
                    {
                        method: "POST",
                        body: utils.getFormData(updatingItem)
                    }
                );
            } else {
                res = await fetch(
                    constants.API_URL + "/" + this.model + "/update",
                    {
                        method: "POST",
                        body: utils.getFormData(updatingItem)
                    }
                );
            }
            let response = await res.json()

            if (response.status === "success") {
                if (isNew) {
                    await this.formRef.current?.setFieldsValue({_id: response.data})
                    let hasUploadField = false;
                    for (let col of this.state.columns) {
                        if (col.dataType === "imageUpload") {
                            hasUploadField = true;
                            break;
                        }
                    }
                    if (hasUploadField) await this.doUpload(this.uploadThis, response.data); //TODO check if upload is working in update
                    await this.setState({
                        data: newData.map((item) => {
                            if (item._id === 0) {
                                item._id = response.data;
                                for (const col of this.state.columns.filter((columns) => columns.dataType === "imageUpload")) {
                                    let fields: any = {};
                                    //@ts-ignore
                                    item[col.dataIndex] = this.formRef.current?.getFieldValue(col.dataIndex);
                                }
                            }
                            return item;
                        })
                    });
                    if (hasUploadField) await this.save(response.data)

                    this.setState({loading: false});
                } else {
                    this.setState({data: newData});
                }
                notification['success']({
                    message: isNew?'Register Success':"Update Success",
                    description: response.message,
                });
                console.log("Data", this.state.data);
                this.setState({editingKey: undefined})
                this.setState({loading: false});
                return;
            } else {
                notification.error({
                    message: 'Error',
                    description: response.message,
                });
                this.setState({loading: false});
                Throw();
            }

        } catch (e) {
            this.setState({loading: false});
            Throw();
        }


    };

    abstract model: string;

    async delete(_id: string | number): Promise<void> {
        this.setState({loading: true});
        const updatingItem = this.state.data.find((item) => item._id === _id);
        if (updatingItem === undefined) {
            return;
        }
        const Throw = () => {
            throw false;
        }

        try {
            let res;
            res = await fetch(
                constants.API_URL + "/delete.php",
                {
                    method: "POST", body: JSON.stringify(
                        {
                            class: this.model,
                            _id: _id
                        })
                }
            );
            let response = await res.json()
            this.setState({loading: false});
            if (response.status === "success") {
                notification['success']({
                    message: 'Delete Success',
                    description: response.message,
                });
                this.setState({data: this.state.data.filter((item) => item._id !== _id)})
                return;
            } else {
                notification.error({
                    message: 'Error',
                    description: response.message,
                });
                Throw();
            }
        } catch (e) {
            this.setState({loading: false});
            Throw();
        }
    }

    cancel = () => {
        this.setState({editingKey: undefined});
    };

    private reloadColumns() {
        this.setColumns(this.state.columns.filter((column) => column.dataType !== "operation"))
    }

    async setColumns(columns: ColumnType[]) {
        const finalColumns = [
            ...columns.map(col => {
                console.log("onCellMap", col.extra);
                return {
                    ...col,
                    render: (value: any, record: Item) => {
                        return <this.EditableCell
                            index={record._id}
                            record={record}
                            value={value}
                            column={col}
                            editing={(col.editable === true && record._id === this.state.editingKey)}/>
                    }
                    // onCell: (record: Item) => {
                    //     console.log("onCell:", record, col)
                    //     return {
                    //         record: record,
                    //         dataType: col.dataType,
                    //         dataIndex: col.dataIndex,
                    //         title: col.title,
                    //         rules: col.rules,
                    //         editing: col.editable && record._id === this.state.editingKey,
                    //         extra: col.extra
                    //     }
                    // }
                };
            }),
            {
                title: 'Operation',
                dataType: 'operation',
                dataIndex: 'operation',
                width: 90,
                rules: [],
                render: (_: any, record: Item) => {
                    return record._id === this.state.editingKey ?
                        (<span>
                        <Button shape="circle" icon={<SaveOutlined/>}
                                onClick={() => this.save(record._id)}
                                style={{marginLeft: 8}} title="Save" className={"success"}/>
                        <Popconfirm title="Are you sure?" onConfirm={() => {
                            record._id !== 0 ? this.cancel() : this.cancelAddItem()
                        }} cancelText="No" okText="Yes">
                            <Button color={"red"} shape="circle" title="Cancel" icon={<CloseOutlined color="red"/>}/>
                        </Popconfirm>
                    </span>)
                        :
                        (<span>
                        <Button
                            shape="circle"
                            icon={<EditOutlined color={"blue"}/>}
                            disabled={this.state.editingKey !== undefined}
                            onClick={() => this.edit(record._id)} style={{marginLeft: 8}} title="Edit"/>
                            {(record._id !== 0) ?
                                (<Popconfirm title="Are you sure?" onConfirm={() => this.delete(record._id)}
                                             cancelText="No"
                                             okText="Yes">
                                    <Button shape="circle" icon={<DeleteOutlined color={"red"}/>} title='Delete'/>
                                </Popconfirm>) : undefined
                            }
                    </span>);
                }
            }]
        const prevColumn = [...this.state.data];
        await this.setState({columns: finalColumns, data: []})
        await this.setState({data: prevColumn})
        this.state.columns.forEach((col) => {
        })
    }

    getColumns(): ColumnType[] {
        return this.state.columns.filter((column) => column.dataType != "operation")
    }

    async componentDidMount() {
        this.setState({loading: true});
    }

    tempItem?: Item;

    addItem() {
        if (this.state.editingKey !== undefined || this.state.data.find(v => v._id === 0)) {
            notification.warn({message: 'Register current item before add another one.'})
            return;
        }

        this.tempItem = this.state.data.find((_, i) => i === 10)

        const data = [{
            _id: 0,
        }, ...this.state.data.filter((_, i) => i < 10)];
        this.formRef.current?.resetFields();
        this.setState({data: data, editingKey: 0});
    }

    cancelAddItem() {
        const data = this.state.data.filter(v => v._id !== 0)
        if (this.tempItem !== undefined) {
            this.state.data.push(this.tempItem)
        }
        this.setState({data: data})
    }

    //formRef = React.createRef<FormInstance>();

    uploadThis: any;
    formRef = React.createRef<FormInstance>();

    /*EditableRow: React.FC<EditableRowProps> = ({ index, ...props } : any) => {
        return (<Form form={form} component={false}  ><tr {...props} /></Form>)
    }*/

    EditableCell: React.FC<EditableCellProps> = ({
                                                     editing,
                                                     record,
                                                     value,
                                                     column,
                                                     ...restProps
                                                 }) => {

        let node: any;
        if (editing) {
            let inputNode: any = <Input/>;
            switch (column.dataType) {
                case "boolean":
                    inputNode = <BooleanSwitch/>
                    break;
                case 'number':
                    inputNode = <InputNumber/>
                    break;
                case 'selection':
                    inputNode = <Select2
                        options={column.extra?.options}
                        objectValue={column.extra?.objectValue}
                    />
                    break;
                case 'cascader':
                    if (column.extra && column.extra.options && column.extra.cascaderLoadData) {
                        column.shouldCellUpdate = (record, preRecord) => {
                            console.log("shouldCellUpdate", record, preRecord)
                            return true;
                        }
                        inputNode = <Cascader2
                            reload={() => {
                                if (typeof column.extra?.reload === "function") column.extra.reload()
                            }}
                            options={column.extra.options}
                            loadData={column.extra.cascaderLoadData}
                            subObjKeys={column.extra.subObjKeys}/>
                    } else {
                        if (value != undefined) {
                            inputNode = value.toString()
                        } else {
                            value = ""
                        }
                    }
                    break;
                case 'imageUpload':
                    inputNode = <Upload2
                        data={{path: (this.model.replace(/^\w/, chr => chr.toLowerCase())) + "/(%_id%)_(%imgVersion%).webp"}}
                        getDoUpload={(doUpload, that) => {
                            this.doUpload = doUpload
                            this.uploadThis = that;
                        }}
                        record={record}
                        previewURL={"images/" + (this.model.replace(/^\w/, chr => chr.toLowerCase())) + "/(%_id%)_(%imgVersion%).webp"}/>
                    break;
                case 'html':
                    let visible = false;
                    inputNode = <BraftModal title='Description'/>
            }
            let finalRules: RuleObject[] = []
            if (column.rules) {
                finalRules = column.rules.map((item: RuleObject) => {
                    if (item.required && !item.message) {
                        item.message = `Please enter the  ${column.title} !`
                    }
                    return item;
                })
            }
            node = <Form.Item
                name={column.dataIndex}
                style={{margin: 0}}
                rules={finalRules}
            >
                {inputNode}
            </Form.Item>;
        } else {
            if (typeof column.render === "function") {
                node = column.render(value, record)
            } else {
                switch (column.dataType) {
                    case "text":
                        node = value
                        break;
                    case "boolean":
                        node = <Switch checked={value}/>
                        break;
                    case 'number':
                        node = String(value)
                        break;
                    case 'selection':
                        if (column.extra?.objectValue) {
                            node = String(value.title)
                        } else {
                            node = column.extra?.options?.find((option) => option.value == value)?.label
                        }
                        break;
                    case 'cascader':
                        let title = value?.title;
                        if (column.extra?.subObjKeys != undefined && column.extra.subObjKeys[1]) {
                            title = value[column.extra.subObjKeys[1]].title + " / " + title;
                            if (column.extra.subObjKeys[1]) {
                                title = value[column.extra?.subObjKeys[1]][column.extra?.subObjKeys[0]].title + " / " + title
                            }
                        } else if (column.extra?.subObjKeys != undefined && column.extra.subObjKeys[0]) {
                            title = value[column.extra.subObjKeys[0]].title + " / " + title;
                        }
                        node = <div>{title}</div>
                        break;
                    case 'imageUpload':
                        node = (
                            <div>
                                <img
                                    style={{height: 100}}
                                    src={'images/' + this.model.replace(/^\w/, chr => chr.toLowerCase()) + '/' + record._id + "_" + record[column.dataIndex] + ".webp"}/>
                            </div>
                        )
                        break;
                    case 'html':
                        node = (
                            <Popover content={() => (<div dangerouslySetInnerHTML={{__html: value}}/>)}>
                                <Button>Description</Button>
                            </Popover>)

                }
            }

        }


        switch (column.dataType) {
            case 'boolean':
                // @ts-ignore
                //;
                break;
        }
        return (
            <td {...restProps}>
                {node}
            </td>
        );
    };

    abstract onChange(pagination: any, filters: any, sorter: any, extra: any): void;

    render() {
        return (
            <Form ref={this.formRef}>
                <Table
                    loading={this.state.loading}
                    bordered
                    dataSource={this.state.data}
                    columns={this.state.columns}
                    rowClassName="editable-row"
                    pagination={this.state.pagination}
                    onChange={(...attrs) => {
                        this.onChange(...attrs)
                    }}
                    rowKey={record => record._id}
                >
                    {
                        this.state.columns.map((col) => (
                            <Table.Column
                                title={col.title}
                                key={col.dataIndex}
                                width={col.width}
                                dataIndex={col.dataIndex}
                            />
                        ))
                    }
                </Table>
            </Form>
        )
    }

}

export default DataTable;
