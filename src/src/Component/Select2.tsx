import React, {Component} from "react";
import {Menu, Dropdown, Button} from "antd";
import {CascaderOptionType} from "antd/lib/cascader";
import {MenuClickEventHandler, MenuInfo} from "rc-menu/lib/interface";
import {LabeledValue} from "antd/lib/select";

type ResultValue = { id: number, title?: string } | number;

export interface OptionFieldProps {
    value?: { title: string, id: number } | number;
    onChange?: (value: ResultValue | null) => void;
    model?: string,
    options?: CascaderOptionType[]
    subObjKeys?: string[],
    objectValue?: boolean
}


export default class Select2 extends Component<OptionFieldProps> {


    state = {
        options: [],
        value: []
    }


    constructor(props: OptionFieldProps) {
        super(props);
        console.log("registered Value", this.props.value)

    }


    triggerChange =
        (value: { value: string, label: string } | string) => {
            console.log("Value", JSON.stringify({item: value}));
            // const v = this.props.options?.map((option) => ({
            //     title: option.label ? option.label.toString() : '',
            //     id: Number.parseInt(typeof option.value === "string" ? option.value : '0')
            // })).find((option) => option.id === Number.parseInt(value.value))
            if (this.props.onChange) {
                if (this.props.objectValue && typeof value === "object") {
                    this.props.onChange({title: value.label, id: Number.parseInt(value.value)});
                } else {
                    if (typeof value === "string")
                        this.props.onChange(Number.parseInt(value));
                }
            }
        };


    get innerValue(): { label: string, value: string } | string {
        console.log("innerValue")
        console.log("options", this.props.options)
        console.log("Value", this.props.value)
        console.log("objectValue", this.props.objectValue)
        if (this.props.objectValue) {
            if (typeof this.props.value === "object")
                return {
                    value: this.props.value ? String(this.props.value.id) : '',
                    label: this.props.value ? this.props.value.title : ""
                }
            return {label: 'انتخاب کنید', value: ''};
        } else {
            const v = this.props.options?.find((option) => option.value == this.props.value);
            console.log("v", v)
            //@ts-ignore
            console.log("value", {label: v?.label ? String(v?.label) : "", value: String(v?.value)})
            return String(v?.value)//{label: v?.label ? String(v?.label) : "", value: String(v?.value)};//{label: v?.label ? String(v?.label) : "", value: String(v?.value)}
        }
    }

    get label(): string {
        if (this.props.objectValue) {
            if(typeof this.props.value ==="object") {
                return this.props.value.title;
            }else{
                return "انتخاب کنید"
            }
        } else {
            let selectedItem = this.props.options?.find((option) => option.value == this.props.value)
            return selectedItem ? (selectedItem.label ? selectedItem.label.toString() : selectedItem.value ? selectedItem.value.toString() : "انتخاب کنید") : "انتخاب کنید"
        }
    }

    selectItem(value: number | string | undefined) {
        if (this.props.onChange) {
            if (this.props.objectValue) {
                let selectedItem = this.props.options?.find((option) => option.value == value)
                console.log("selectedItem:", selectedItem, value);
                if (selectedItem)
                    this.props.onChange({
                        title: selectedItem.label?.toString(),
                        id: Number.parseInt(selectedItem.value ? selectedItem.value.toString() : '0')
                    });
            } else {
                this.props.onChange(Number.parseInt(value ? value.toString() : '0'));
            }
        }
    }

    menu() {
        return 0
    }

    render() {
        console.log("Select2 options", this.props.options)
        return (
            <Dropdown overlay={() => (
                <Menu>{
                    this.props.options?.map((option) =>
                        <Menu.Item key={option.value} onClick={(info: MenuInfo) => this.selectItem(info.key)}>
                            {option.label}
                        </Menu.Item>
                    )}
                </Menu>)
            } placement="bottomLeft" arrow mouseEnterDelay={0} trigger={["click","hover"]}>
                <Button>{this.label}</Button>
            </Dropdown>
        )
    }
}
