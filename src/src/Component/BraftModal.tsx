import React, {Component} from "react";
import {Button, Modal} from "antd";
import BraftEditor from "braft-editor";

export interface OptionFieldProps {
    value?: String | number;
    onChange?: (value: String | null) => void;
    title: string
}


export default class BraftModal extends Component<OptionFieldProps> {


    state = {
        modalVisibility: false,
        editorState: BraftEditor.createEditorState(this.props.value)
    }

    componentDidMount(){
        this.setState({editorState: BraftEditor.createEditorState(this.props.value)})
    }


    render() {
        return (
            <>
                <Button onClick={() => {
                    this.setState({modalVisibility: true})
                }}>توضیحات</Button>
                <Modal
                    title={this.props.title}
                    centered
                    visible={this.state.modalVisibility}
                    onOk={() => {
                        if (this.props.onChange) this.props.onChange(this.state.editorState.toHTML());                        this.setState({modalVisibility: false})}}
                    onCancel={() => this.setState({modalVisibility: false})}
                    width={1000}
                >
                    <BraftEditor
                        language={(languages) => {
                            console.log(languages.en);
                            const fa = Object.fromEntries(Object.entries(languages.en as object).map(([k,v])=>([k,v])))
                            return fa;
                        }}
                        value={this.state.editorState}
                        onChange={(editorState) => {
                            this.setState({editorState: editorState})
                        }}
                    />
                </Modal>
            </>
        )
    }
}
