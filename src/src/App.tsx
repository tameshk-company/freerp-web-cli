import React from 'react';
import './App.css';
import {
    Switch,
    Route,
    BrowserRouter as Router,
} from "react-router-dom";
import Login from "./View/Login";
import Currency from "./View/Currency";
import Dashboard from "./View/Dashboard";
import {Layout, Menu} from "antd";

const {Header, Content, Sider} = Layout;


function App() {
    return (
        <Layout style={{height: "100%"}}>
            <Content
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                    height: "100%"
                }}
            >
                <Router>
                    <Switch>
                        <Route path="/login">
                            <Login/>
                        </Route>
                        <Route path="/currency">
                            <Currency/>
                        </Route>
                        <Route path="/">
                            <Dashboard/>
                        </Route>
                    </Switch>
                </Router>
            </Content>
        </Layout>
    );
}

export default App;
